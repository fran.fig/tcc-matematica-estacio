# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#%%
import numpy as np
import pandas as pd 
import gc
import locale
import os.path
from os import path
from pathlib import Path

from timeit import default_timer as timer
from datetime import timedelta

#%%
pastaResultados = '1G8cCwrJUh3MKVmgefKshj6qyH9BruWR9';  
ANO = 2019

#%%
#downloaded = drive.CreateFile({'id':idArqTeste}) 
#downloaded = drive.CreateFile({'id':pastaResultados})
#downloaded.GetContentFile('MICRODADOS_ENEM_2019.csv')  
#downloaded.clear();
#del downloaded
#cp '/content/drive/MyDrive/Colab Notebooks/microdados_enem2019/DADOS/MICRODADOS_ENEM_2019.csv' .
#dados_enem = pd.read_csv('MICRODADOS_ENEM_2019.csv',sep=";", encoding='latin-1',dtype= mapa_tipos)
#dados_enem = pd.read_csv('/content/drive/MyDrive/Colab Notebooks/microdados_enem2019/DADOS/MICRODADOS_ENEM_2019.csv',sep=";", encoding='latin-1',dtype= mapa_tipos)
#dataFile = '/content/drive/MyDrive/Colab Notebooks/microdados_enem2019/DADOS/MICRODADOS_ENEM_2019.csv'
folderData = 't:/$Nuvem/GoogleDrive/Colab Notebooks/microdados_enem2019' 
if path.exists('/content/drive/MyDrive/Colab Notebooks/microdados_enem2019'):
    folderData = '/content/drive/MyDrive/Colab Notebooks/microdados_enem2019'

if not path.exists(folderData):
    raise Exception("Sorry, no folder Data")
    
dataFile = folderData + '/DADOS/MICRODADOS_ENEM_2019.csv'

print(dataFile)

#%%
mapa_tipos = {
    'NU_INSCRICAO' : 'Int64',
    'NU_ANO' : 'Int16',
    'CO_MUNICIPIO_RESIDENCIA': 'Int32',
    'NO_MUNICIPIO_RESIDENCIA': 'string',
    'CO_UF_RESIDENCIA': 'Int8',
    'SG_UF_RESIDENCIA': 'string',
    'NU_IDADE': 'Int8',
    'TP_SEXO': pd.CategoricalDtype(['M','F']),
    'TP_ESTADO_CIVIL': pd.CategoricalDtype(['0', '1', '2', '3', '4']),
    'TP_COR_RACA': pd.CategoricalDtype(['0', '1', '2', '3', '4', '5']),
    'TP_NACIONALIDADE': pd.CategoricalDtype(['0', '1', '2', '3', '4']),
    'CO_MUNICIPIO_NASCIMENTO': 'string',
    'NO_MUNICIPIO_NASCIMENTO': 'string',
    'CO_UF_NASCIMENTO': 'string',
    'SG_UF_NASCIMENTO': 'string',
    'TP_ST_CONCLUSAO': pd.CategoricalDtype(['1', '2', '3', '4']) ,
    'TP_ANO_CONCLUIU': pd.CategoricalDtype(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13']),
    'TP_ESCOLA': pd.CategoricalDtype(['1', '2', '3', '4']),
    'TP_ENSINO': pd.CategoricalDtype(['1', '2', '3']) ,
    'IN_TREINEIRO': 'Int8',
    'CO_ESCOLA': 'string',
    'CO_MUNICIPIO_ESC': 'string',
    'NO_MUNICIPIO_ESC': 'string',
    'CO_UF_ESC': 'string',
    'SG_UF_ESC': pd.CategoricalDtype(['1', '2', '3', '4']) ,
    'TP_DEPENDENCIA_ADM_ESC': pd.CategoricalDtype(['1', '2', '3', '4']) ,
    'TP_LOCALIZACAO_ESC': pd.CategoricalDtype(['1', '2']) ,
    'TP_SIT_FUNC_ESC': pd.CategoricalDtype(['1', '2', '3']),
    'IN_BAIXA_VISAO': 'Int8' ,
    'IN_CEGUEIRA': 'Int8' ,
    'IN_SURDEZ': 'Int8' ,
    'IN_DEFICIENCIA_AUDITIVA': 'Int8' ,
    'IN_SURDO_CEGUEIRA': 'Int8' ,
    'IN_DEFICIENCIA_FISICA': 'Int8' ,
    'IN_DEFICIENCIA_MENTAL': 'Int8' ,
    'IN_DEFICIT_ATENCAO': 'Int8' ,
    'IN_DISLEXIA': 'Int8' ,
    'IN_DISCALCULIA': 'Int8' ,
    'IN_AUTISMO': 'Int8' ,
    'IN_VISAO_MONOCULAR': 'Int8' ,
    'IN_OUTRA_DEF': 'Int8' ,
    'IN_GESTANTE': 'Int8' ,
    'IN_LACTANTE': 'Int8' ,
    'IN_IDOSO': 'Int8' ,
    'IN_ESTUDA_CLASSE_HOSPITALAR': 'Int8' ,
    'IN_SEM_RECURSO': 'Int8',
    'IN_BRAILLE': 'Int8',
    'IN_AMPLIADA_24': 'Int8',
    'IN_AMPLIADA_18': 'Int8',
    'IN_LEDOR': 'Int8',
    'IN_ACESSO': 'Int8',
    'IN_TRANSCRICAO': 'Int8',
    'IN_LIBRAS': 'Int8',
    'IN_TEMPO_ADICIONAL': 'Int8',
    'IN_LEITURA_LABIAL': 'Int8',
    'IN_MESA_CADEIRA_RODAS': 'Int8',
    'IN_MESA_CADEIRA_SEPARADA': 'Int8',
    'IN_APOIO_PERNA': 'Int8',
    'IN_GUIA_INTERPRETE': 'Int8',
    'IN_COMPUTADOR': 'Int8',
    'IN_CADEIRA_ESPECIAL': 'Int8',
    'IN_CADEIRA_CANHOTO': 'Int8',
    'IN_CADEIRA_ACOLCHOADA': 'Int8',
    'IN_PROVA_DEITADO': 'Int8',
    'IN_MOBILIARIO_OBESO': 'Int8',
    'IN_LAMINA_OVERLAY': 'Int8',
    'IN_PROTETOR_AURICULAR': 'Int8',
    'IN_MEDIDOR_GLICOSE': 'Int8',
    'IN_MAQUINA_BRAILE': 'Int8',
    'IN_SOROBAN': 'Int8',
    'IN_MARCA_PASSO': 'Int8',
    'IN_SONDA': 'Int8',
    'IN_MEDICAMENTOS': 'Int8',
    'IN_SALA_INDIVIDUAL': 'Int8',
    'IN_SALA_ESPECIAL': 'Int8',
    'IN_SALA_ACOMPANHANTE': 'Int8',
    'IN_MOBILIARIO_ESPECIFICO': 'Int8',
    'IN_MATERIAL_ESPECIFICO': 'Int8',
    'IN_NOME_SOCIAL': 'Int8',
    'CO_MUNICIPIO_PROVA': 'Int32',
    'NO_MUNICIPIO_PROVA': 'string',
    'CO_UF_PROVA': 'Int8',
    'SG_UF_PROVA': 'string',
    'TP_PRESENCA_CN': pd.CategoricalDtype(['0', '1', '2']),
    'TP_PRESENCA_CH': pd.CategoricalDtype(['0', '1', '2']),
    'TP_PRESENCA_LC': pd.CategoricalDtype(['0', '1', '2']),
    'TP_PRESENCA_MT': pd.CategoricalDtype(['0', '1', '2']), 
    'CO_PROVA_CN': 'Int16',
    'CO_PROVA_CH': 'Int16',
    'CO_PROVA_LC': 'Int16',
    'CO_PROVA_MT': 'Int16',
    'NU_NOTA_CN': 'float64',
    'NU_NOTA_CH': 'float64',
    'NU_NOTA_LC': 'float64',
    'NU_NOTA_MT': 'float64',
    'TX_RESPOSTAS_CN': 'string',
    'TX_RESPOSTAS_CH': 'string',
    'TX_RESPOSTAS_LC': 'string',
    'TX_RESPOSTAS_MT': 'string',
    'TP_LINGUA' : pd.CategoricalDtype(['0', '1']), 
    'TX_GABARITO_CN' : 'string',
    'TX_GABARITO_CH' : 'string',
    'TX_GABARITO_LC' : 'string',
    'TX_GABARITO_MT' : 'string',
    'TP_STATUS_REDACAO' : pd.CategoricalDtype(['1', '2', '3', '4', '6', '7', '8', '9']), 
    'NU_NOTA_COMP1': 'float64',
    'NU_NOTA_COMP2': 'float64',
    'NU_NOTA_COMP3': 'float64',
    'NU_NOTA_COMP4': 'float64',
    'NU_NOTA_COMP5': 'float64',
    'NU_NOTA_REDACAO' : 'float64',
    'Q001' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']) ,
    'Q002' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']) ,
    'Q003' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F']) ,
    'Q004' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F']) ,
    'Q005' : 'Int8',
    'Q006' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q']) ,
    'Q007' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q008' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q009' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q010' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q011' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q012' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q013' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q014' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q015' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q016' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q017' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q018' : pd.CategoricalDtype(['A', 'B']) ,
    'Q019' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q020' : pd.CategoricalDtype(['A', 'B']) ,
    'Q021' : pd.CategoricalDtype(['A', 'B']) ,
    'Q022' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q023' : pd.CategoricalDtype(['A', 'B']) ,
    'Q024' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
    'Q025' : pd.CategoricalDtype(['A', 'B'])
}

#%%
dados_enem = pd.read_csv(dataFile,sep=";", encoding='latin-1',dtype= mapa_tipos)

#%%
dados_enem['IN_TREINEIRO'                ] = dados_enem['IN_TREINEIRO'                ].astype('boolean')
dados_enem['IN_BAIXA_VISAO'              ] = dados_enem['IN_BAIXA_VISAO'              ].astype('boolean')
dados_enem['IN_CEGUEIRA'                 ] = dados_enem['IN_CEGUEIRA'                 ].astype('boolean')
dados_enem['IN_SURDEZ'                   ] = dados_enem['IN_SURDEZ'                   ].astype('boolean')
dados_enem['IN_DEFICIENCIA_AUDITIVA'     ] = dados_enem['IN_DEFICIENCIA_AUDITIVA'     ].astype('boolean')
dados_enem['IN_SURDO_CEGUEIRA'           ] = dados_enem['IN_SURDO_CEGUEIRA'           ].astype('boolean')
dados_enem['IN_DEFICIENCIA_FISICA'       ] = dados_enem['IN_DEFICIENCIA_FISICA'       ].astype('boolean')
dados_enem['IN_DEFICIENCIA_MENTAL'       ] = dados_enem['IN_DEFICIENCIA_MENTAL'       ].astype('boolean')
dados_enem['IN_DEFICIT_ATENCAO'          ] = dados_enem['IN_DEFICIT_ATENCAO'          ].astype('boolean')
dados_enem['IN_DISLEXIA'                 ] = dados_enem['IN_DISLEXIA'                 ].astype('boolean')
dados_enem['IN_DISCALCULIA'              ] = dados_enem['IN_DISCALCULIA'              ].astype('boolean')
dados_enem['IN_AUTISMO'                  ] = dados_enem['IN_AUTISMO'                  ].astype('boolean')
dados_enem['IN_VISAO_MONOCULAR'          ] = dados_enem['IN_VISAO_MONOCULAR'          ].astype('boolean')
dados_enem['IN_OUTRA_DEF'                ] = dados_enem['IN_OUTRA_DEF'                ].astype('boolean')
dados_enem['IN_GESTANTE'                 ] = dados_enem['IN_GESTANTE'                 ].astype('boolean')
dados_enem['IN_LACTANTE'                 ] = dados_enem['IN_LACTANTE'                 ].astype('boolean')
dados_enem['IN_IDOSO'                    ] = dados_enem['IN_IDOSO'                    ].astype('boolean')
dados_enem['IN_ESTUDA_CLASSE_HOSPITALAR' ] = dados_enem['IN_ESTUDA_CLASSE_HOSPITALAR' ].astype('boolean')
dados_enem['IN_SEM_RECURSO'              ] = dados_enem['IN_SEM_RECURSO'              ].astype('boolean')
dados_enem['IN_BRAILLE'                  ] = dados_enem['IN_BRAILLE'                  ].astype('boolean')
dados_enem['IN_AMPLIADA_24'              ] = dados_enem['IN_AMPLIADA_24'              ].astype('boolean')
dados_enem['IN_AMPLIADA_18'              ] = dados_enem['IN_AMPLIADA_18'              ].astype('boolean')
dados_enem['IN_LEDOR'                    ] = dados_enem['IN_LEDOR'                    ].astype('boolean')
dados_enem['IN_ACESSO'                   ] = dados_enem['IN_ACESSO'                   ].astype('boolean')
dados_enem['IN_TRANSCRICAO'              ] = dados_enem['IN_TRANSCRICAO'              ].astype('boolean')
dados_enem['IN_LIBRAS'                   ] = dados_enem['IN_LIBRAS'                   ].astype('boolean')
dados_enem['IN_TEMPO_ADICIONAL'          ] = dados_enem['IN_TEMPO_ADICIONAL'          ].astype('boolean')
dados_enem['IN_LEITURA_LABIAL'           ] = dados_enem['IN_LEITURA_LABIAL'           ].astype('boolean')
dados_enem['IN_MESA_CADEIRA_RODAS'       ] = dados_enem['IN_MESA_CADEIRA_RODAS'       ].astype('boolean')
dados_enem['IN_MESA_CADEIRA_SEPARADA'    ] = dados_enem['IN_MESA_CADEIRA_SEPARADA'    ].astype('boolean')
dados_enem['IN_APOIO_PERNA'              ] = dados_enem['IN_APOIO_PERNA'              ].astype('boolean')
dados_enem['IN_GUIA_INTERPRETE'          ] = dados_enem['IN_GUIA_INTERPRETE'          ].astype('boolean')
dados_enem['IN_COMPUTADOR'               ] = dados_enem['IN_COMPUTADOR'               ].astype('boolean')
dados_enem['IN_CADEIRA_ESPECIAL'         ] = dados_enem['IN_CADEIRA_ESPECIAL'         ].astype('boolean')
dados_enem['IN_CADEIRA_CANHOTO'          ] = dados_enem['IN_CADEIRA_CANHOTO'          ].astype('boolean')
dados_enem['IN_CADEIRA_ACOLCHOADA'       ] = dados_enem['IN_CADEIRA_ACOLCHOADA'       ].astype('boolean')
dados_enem['IN_PROVA_DEITADO'            ] = dados_enem['IN_PROVA_DEITADO'            ].astype('boolean')
dados_enem['IN_MOBILIARIO_OBESO'         ] = dados_enem['IN_MOBILIARIO_OBESO'         ].astype('boolean')
dados_enem['IN_LAMINA_OVERLAY'           ] = dados_enem['IN_LAMINA_OVERLAY'           ].astype('boolean')
dados_enem['IN_PROTETOR_AURICULAR'       ] = dados_enem['IN_PROTETOR_AURICULAR'       ].astype('boolean')
dados_enem['IN_MEDIDOR_GLICOSE'          ] = dados_enem['IN_MEDIDOR_GLICOSE'          ].astype('boolean')
dados_enem['IN_MAQUINA_BRAILE'           ] = dados_enem['IN_MAQUINA_BRAILE'           ].astype('boolean')
dados_enem['IN_SOROBAN'                  ] = dados_enem['IN_SOROBAN'                  ].astype('boolean')
dados_enem['IN_MARCA_PASSO'              ] = dados_enem['IN_MARCA_PASSO'              ].astype('boolean')
dados_enem['IN_SONDA'                    ] = dados_enem['IN_SONDA'                    ].astype('boolean')
dados_enem['IN_MEDICAMENTOS'             ] = dados_enem['IN_MEDICAMENTOS'             ].astype('boolean')
dados_enem['IN_SALA_INDIVIDUAL'          ] = dados_enem['IN_SALA_INDIVIDUAL'          ].astype('boolean')
dados_enem['IN_SALA_ESPECIAL'            ] = dados_enem['IN_SALA_ESPECIAL'            ].astype('boolean')
dados_enem['IN_SALA_ACOMPANHANTE'        ] = dados_enem['IN_SALA_ACOMPANHANTE'        ].astype('boolean')
dados_enem['IN_MOBILIARIO_ESPECIFICO'    ] = dados_enem['IN_MOBILIARIO_ESPECIFICO'    ].astype('boolean')
dados_enem['IN_MATERIAL_ESPECIFICO'      ] = dados_enem['IN_MATERIAL_ESPECIFICO'      ].astype('boolean')
dados_enem['IN_NOME_SOCIAL'              ] = dados_enem['IN_NOME_SOCIAL'              ].astype('boolean')

dados_enem['TP_ESTADO_CIVIL'       ] = dados_enem['TP_ESTADO_CIVIL'       ].cat.rename_categories({'0' : 'Não informado', '1' : 'Solteiro(a)', '2' : 'Casado(a)/Mora com companheiro(a)', '3' : 'Divorciado(a)/Desquitado(a)/Separado(a)', '4' : 'Viúvo(a)'})
dados_enem['TP_COR_RACA'           ] = dados_enem['TP_COR_RACA'           ].cat.rename_categories({'0' : 'Não declarado','1' : 'Branca','2' : 'Preta','3' : 'Parda','4' : 'Amarela','5' : 'Indígena '})
dados_enem['TP_NACIONALIDADE'      ] = dados_enem['TP_NACIONALIDADE'      ].cat.rename_categories({'0' : 'Não informado','1' : 'Brasileiro(a)','2' : 'Brasileiro(a) Naturalizado(a)','3' : 'Estrangeiro(a)','4' : 'Brasileiro(a) Nato(a), nascido(a) no exterior'})
dados_enem['TP_ST_CONCLUSAO'       ] = dados_enem['TP_ST_CONCLUSAO'       ].cat.rename_categories({'1' : 'Já concluí o Ensino Médio','2' : 'Estou cursando e concluirei o Ensino Médio em 2019','3' : 'Estou cursando e concluirei o Ensino Médio após 2019','4' : 'Não concluí e não estou cursando o Ensino Médio',})
dados_enem['TP_ANO_CONCLUIU'       ] = dados_enem['TP_ANO_CONCLUIU'       ].cat.rename_categories({'0' : 'Não informado','1' : '2018','2' : '2017','3' : '2016','4' : '2015','5' : '2014','6' : '2013','7' : '2012','8' : '2011','9' : '2010','10' : '2009','11' : '2008','12' : '2007','13' : 'Antes de 2007'})
dados_enem['TP_ESCOLA'             ] = dados_enem['TP_ESCOLA'             ].cat.rename_categories({'1' : 'Não Respondeu','2' : 'Pública','3' : 'Privada','4' : 'Exterior'})
dados_enem['TP_ENSINO'             ] = dados_enem['TP_ENSINO'             ].cat.rename_categories({'1' : 'Ensino Regular','2' : 'Educação Especial - Modalidade Substitutiva','3' : 'Educação de Jovens e Adultos'})


dados_enem['TP_DEPENDENCIA_ADM_ESC'] = dados_enem['TP_DEPENDENCIA_ADM_ESC'].cat.rename_categories({'1' : 'Federal','2' : 'Estadual','3' : 'Municipal','4' : 'Privada'})
dados_enem['TP_LOCALIZACAO_ESC'    ] = dados_enem['TP_LOCALIZACAO_ESC'    ].cat.rename_categories({'1' : 'Urbana','2' : 'Rural'})
dados_enem['TP_SIT_FUNC_ESC'       ] = dados_enem['TP_SIT_FUNC_ESC'       ].cat.rename_categories({'1' : 'Em atividade','2' : 'Paralisada','3' : 'Extinta'})

dados_enem['TP_PRESENCA_CN'        ] = dados_enem['TP_PRESENCA_CN'        ].cat.rename_categories({'0' : 'Faltou à prova','1' : 'Presente na prova','2' : 'Eliminado na prova'})                
dados_enem['TP_PRESENCA_CH'        ] = dados_enem['TP_PRESENCA_CH'        ].cat.rename_categories({'0' : 'Faltou à prova','1' : 'Presente na prova','2' : 'Eliminado na prova'})                                
dados_enem['TP_PRESENCA_LC'        ] = dados_enem['TP_PRESENCA_LC'        ].cat.rename_categories({'0' : 'Faltou à prova','1' : 'Presente na prova','2' : 'Eliminado na prova'})                                
dados_enem['TP_PRESENCA_MT'        ] = dados_enem['TP_PRESENCA_MT'        ].cat.rename_categories({'0' : 'Faltou à prova','1' : 'Presente na prova','2' : 'Eliminado na prova'})  


dados_enem['TP_LINGUA'             ] = dados_enem['TP_LINGUA'             ].cat.rename_categories({'0' : 'Inglês','1' : 'Espanhol'})  	
dados_enem['TP_STATUS_REDACAO'     ] = dados_enem['TP_STATUS_REDACAO'     ].cat.rename_categories({'1' : 'Sem problemas','2' : 'Anulada','3' : 'Cópia Texto Motivador','4' : 'Em Branco','6' : 'Fuga ao tema','7' : 'Não atendimento ao tipo textual','8' : 'Texto insuficiente','9' : 'Parte desconectada'})  	

dados_enem['Q001'                  ] = dados_enem['Q001'                  ].cat.rename_categories({'A' : 'Nunca estudou.','B' : 'Não completou a 4ª série/5º ano do Ensino Fundamental.','C' : 'Completou a 4ª série/5º ano, mas não completou a 8ª série/9º ano doEnsino Fundamental.','D' : 'Completou a 8ª série/9º ano do Ensino Fundamental, mas não completou o Ensino Médio.','E' : 'Completou o Ensino Médio, mas não completou a Faculdade.','F' : 'Completou a Faculdade, mas não completou a Pós-graduação.','G' : 'Completou a Pós-graduação.','H' : 'Não sei.'})
dados_enem['Q002'                  ] = dados_enem['Q002'                  ].cat.rename_categories({'A' : 'Nunca estudou.','B' : 'Não completou a 4ª série/5º ano do Ensino Fundamental.','C' : 'Completou a 4ª série/5º ano, mas não completou a 8ª série/9º ano do Ensino Fundamental.','D' : 'Completou a 8ª série/9º ano do Ensino Fundamental, mas não completou o Ensino Médio.','E' : 'Completou o Ensino Médio, mas não completou a Faculdade.','F' : 'Completou a Faculdade, mas não completou a Pós-graduação.','G' : 'Completou a Pós-graduação.','H' : 'Não sei.'})
dados_enem['Q003'                  ] = dados_enem['Q003'                  ].cat.rename_categories({'A' : 'Grupo 1: Lavrador, agricultor sem empregados, bóia fria, criador de animais (gado, porcos, galinhas, ovelhas, cavalos etc.), apicultor, pescador, lenhador, seringueiro, extrativista.','B' : 'Grupo 2: Diarista, empregado doméstico, cuidador de idosos, babá, cozinheiro (em casas particulares), motorista particular, jardineiro, faxineiro de empresas e prédios, vigilante, porteiro, carteiro, office-boy, vendedor, caixa, atendente de loja, auxiliar administrativo, recepcionista, servente de pedreiro, repositor de mercadoria.','C' : 'Grupo 3: Padeiro, cozinheiro industrial ou em restaurantes, sapateiro, costureiro, joalheiro, torneiro mecânico, operador de máquinas, soldador, operário de fábrica, trabalhador da mineração, pedreiro, pintor, eletricista, encanador, motorista, caminhoneiro, taxista.','D' : 'Grupo 4: Professor (de ensino fundamental ou médio, idioma, música, artes etc.), técnico (de enfermagem, contabilidade, eletrônica etc.), policial, militar de baixa patente (soldado, cabo, sargento), corretor de imóveis, supervisor, gerente, mestre de obras, pastor, microempresário (proprietário de empresa com menos de 10 empregados), pequeno comerciante, pequeno proprietário de terras, trabalhador autônomo ou por conta própria.','E' : 'Grupo 5: Médico, engenheiro, dentista, psicólogo, economista, advogado, juiz, promotor, defensor, delegado, tenente, capitão, coronel, professor universitário, diretor em empresas públicas ou privadas, político, proprietário de empresas com mais de 10 empregados.','F' : 'Não sei.'})
dados_enem['Q004'                  ] = dados_enem['Q004'                  ].cat.rename_categories({'A' : 'Grupo 1: Lavradora, agricultora sem empregados, bóia fria, criadora de animais (gado, porcos, galinhas, ovelhas, cavalos etc.), apicultora, pescadora, lenhadora, seringueira, extrativista.','B' : 'Grupo 2: Diarista, empregada doméstica, cuidadora de idosos, babá, cozinheira (em casas particulares), motorista particular, jardineira, faxineira de empresas e prédios, vigilante, porteira, carteira, office-boy, vendedora, caixa, atendente de loja, auxiliar administrativa, recepcionista, servente de pedreiro, repositora de mercadoria.','C' : 'Grupo 3: Padeira, cozinheira industrial ou em restaurantes, sapateira, costureira, joalheira, torneira mecânica, operadora de máquinas, soldadora, operária de fábrica, trabalhadora da mineração, pedreira, pintora, eletricista, encanadora, motorista, caminhoneira, taxista.','D' : 'Grupo 4: Professora (de ensino fundamental ou médio, idioma, música, artes etc.), técnica (de enfermagem, contabilidade, eletrônica etc.), policial, militar de baixa patente (soldado, cabo, sargento), corretora de imóveis, supervisora, gerente, mestre de obras, pastora, microempresária (proprietária de empresa com menos de 10 empregados), pequena comerciante, pequena proprietária de terras, trabalhadora autônoma ou por conta própria.','E' : 'Grupo 5: Médica, engenheira, dentista, psicóloga, economista, advogada, juíza, promotora, defensora, delegada, tenente, capitã, coronel, professora universitária, diretora em empresas públicas ou privadas, política, proprietária de empresas com mais de 10 empregados.','F' : 'Não sei.'})    
dados_enem['Q006'                  ] = dados_enem['Q006'                  ].cat.rename_categories({'A' : 'Nenhuma renda.','B' : 'Até R$ 998,00.','C' : 'De R$ 998,01 até R$ 1.497,00.','D' : 'De R$ 1.497,01 até R$ 1.996,00.','E' : 'De R$ 1.996,01 até  R$2.495,00.','F' : 'De R$ 2.495,01 até R$ 2.994,00.','G' : 'De R$ 2.994,01 até R$ 3.992,00.','H' : 'De R$ 3.992,01 até R$ 4.990,00.','I' : 'De R$ 4.990,01 até R$ 5.988,00.','J' : 'De R$ 5.988,01 até R$ 6.986,00.','K' : 'De R$ 6.986,01 até R$ 7.984,00.','L' : 'De R$ 7.984,01 até R$ 8.982,00.','M' : 'De R$ 8.982,01 até R$ 9.980,00.','N' : 'De R$ 9.980,01 até R$ 11.976,00.','O' : 'De R$ 11.976,01 até R$ 14.970,00.','P' : 'De R$ 14.970,01 até R$ 19.960,00.','Q' : 'Mais de R$ 19.960,00.'})

dados_enem['Q007'                  ] = dados_enem['Q007'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um ou dois dias por semana.','C' : 'Sim, três ou quatro dias por semana.','D' : 'Sim, pelo menos cinco dias por semana.'})

dados_enem['Q008'                  ] = dados_enem['Q008'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q009'                  ] = dados_enem['Q009'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q010'                  ] = dados_enem['Q010'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q011'                  ] = dados_enem['Q011'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q012'                  ] = dados_enem['Q012'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q013'                  ] = dados_enem['Q013'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q014'                  ] = dados_enem['Q014'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q015'                  ] = dados_enem['Q015'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q016'                  ] = dados_enem['Q016'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q017'                  ] = dados_enem['Q017'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q018'                  ] = dados_enem['Q018'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim.'})
dados_enem['Q019'                  ] = dados_enem['Q019'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q020'                  ] = dados_enem['Q020'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim.'})
dados_enem['Q021'                  ] = dados_enem['Q021'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim.'})
dados_enem['Q022'                  ] = dados_enem['Q022'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q023'                  ] = dados_enem['Q023'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim.'})
dados_enem['Q024'                  ] = dados_enem['Q024'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim, um.','C' : 'Sim, dois.','D' : 'Sim, três.','E' : 'Sim, quatro ou mais.'})
dados_enem['Q025'                  ] = dados_enem['Q025'                  ].cat.rename_categories({'A' : 'Não.','B' : 'Sim.'})


#%%
Path(folderData + '/Estudo-Enem').mkdir(parents=True, exist_ok=True)
dados_enem.to_pickle(folderData + '/Estudo-Enem/microdados_ENEM_2019.pkl')
#dados_enem.to_pickle('microdados_ENEM_2019.pkl')


#%%
#dados_enem = pd.read_pickle('microdados_ENEM_2019.pkl');
dados_enem = pd.read_pickle(folderData + '/Estudo-Enem/microdados_ENEM_2019.pkl');


#%%
gc.collect()

#%%
print(dados_enem['TP_COR_RACA'].value_counts())

#%%
#pd.options.display.float_format = '${:,.2f}'.format
#!sudo cat /usr/share/i18n/SUPPORTED | grep pt_BR
#!sudo dpkg-reconfigure locales
#import locale
#locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
#pd.options.display.float_format = '{:,.2f}%'.format
#pd.options.display.float_format = ''
print(pd.crosstab(dados_enem['TP_COR_RACA'], dados_enem['TP_PRESENCA_MT'])) #.style.format('{0:,.0f}')

#---------------------------------------------------------------------------------------------


#%%
dados_enem_mt = dados_enem[['NU_INSCRICAO','CO_PROVA_MT','TX_RESPOSTAS_MT','TX_GABARITO_MT','NU_NOTA_MT']][dados_enem['CO_PROVA_MT'].notnull()].reset_index()

#%%
print(dados_enem_mt)

#%%
#start = timer()
#cc = 0
#dados_enem_mt_questoes_mat = dados_enem[['NU_INSCRICAO','CO_PROVA_MT','TX_RESPOSTAS_MT','TX_GABARITO_MT','NU_NOTA_MT']][dados_enem['CO_PROVA_MT'].notnull()].reset_index() #[0:10]
#dados_enem_mt_questoes_decoded = pd.DataFrame(columns=['NU_INSCRICAO','CO_PROVA_MT','CO_POSICAO','CO_RESPOSTAS_MT','CO_GABARITO_MT','IN_CERTO','NU_NOTA_MT'])
#lines = len(dados_enem_mt_questoes_mat.index);
#print('{:,}'.format(lines).replace(',','.') + " linhas ")
#for i, r in dados_enem_mt_questoes_mat.iterrows():
#    respostas = [c for c in dados_enem_mt_questoes_mat.iloc[i]['TX_RESPOSTAS_MT']]
#    gabaritos = [c for c in dados_enem_mt_questoes_mat.iloc[i]['TX_GABARITO_MT']]
#    codigo = [c for c in range(136,181)]
#    for j in range(len(gabaritos)):
#        dados_enem_mt_questoes_decoded = dados_enem_mt_questoes_decoded.append({
#          'NU_INSCRICAO'    : dados_enem_mt_questoes_mat.iloc[i]['NU_INSCRICAO'],
#          'CO_PROVA_MT'     : dados_enem_mt_questoes_mat.iloc[i]['CO_PROVA_MT'],
#          'CO_POSICAO'      : codigo[j],
#          'CO_RESPOSTAS_MT' : respostas[j],
#          'CO_GABARITO_MT'  : gabaritos[j],
#          'IN_CERTO'        : 1 if respostas[j] == gabaritos[j] else 0,
#          'NU_NOTA_MT'      : dados_enem_mt_questoes_mat.iloc[i]['NU_NOTA_MT']
#        }, ignore_index=True)
#        
#    cc = cc + 1
#    if cc % 100 == 0:
#        elapsed  = timer()-start
#        estimed = (elapsed / cc) * (lines)
#        print(" interação " + '{:,}'.format(cc).replace(',','.') + 
#              " de " + '{:,}'.format(lines).replace(',','.') + " linhas " 
#              + " - " + "{0:.2%}".format(cc / lines) 
#              + " / " + '{:,}'.format(len(dados_enem_mt_questoes_decoded.index)).replace(',','.') + ' = '
#              + str(timedelta(seconds=elapsed)) + " => "
#              + str(timedelta(seconds=estimed)) 
#              )
#            
#pd.set_option('display.max_rows', 20)
#dados_enem_mt_questoes_decoded.to_pickle('/content/drive/My Drive/Colab Notebooks/Estudo-Enem/microdados_ENEM_2019.pkl')
#dados_enem_mt_questoes_decoded

#%%
dados_enem_mt_questoes_mat = dados_enem[['NU_INSCRICAO','CO_PROVA_MT','NU_NOTA_MT']][dados_enem['CO_PROVA_MT'].notnull()].reset_index()
dados_enem_mt_questoes_mat['TX_RESPOSTAS_MT'] = dados_enem[['CO_PROVA_MT','TX_RESPOSTAS_MT']][dados_enem['CO_PROVA_MT'].notnull()].reset_index()['TX_RESPOSTAS_MT'].apply(lambda x : [c for c in x])
dados_enem_mt_questoes_mat['TX_GABARITO_MT'] = dados_enem[['CO_PROVA_MT','TX_GABARITO_MT']][dados_enem['CO_PROVA_MT'].notnull()].reset_index()['TX_GABARITO_MT'].apply(lambda x : [c for c in x])
dados_enem_mt_questoes_mat['CO_POSICAO'] = dados_enem[['CO_PROVA_MT']][dados_enem['CO_PROVA_MT'].notnull()].reset_index()['CO_PROVA_MT'].apply(lambda x : [c for c in range(136,181)])
dados_enem_mt_questoes_mat = dados_enem_mt_questoes_mat.apply( pd.Series.explode )
#dados_enem_mt_questoes_mat['CO_U'] = dados_enem_mt_questoes_mat.apply(lambda x : str(ANO) + 'MT' + ('0000' + str(x['CO_PROVA_MT']))[-4:] + ('0000' + str(x['CO_POSICAO']))[-3:] , axis=1) 
dados_enem_mt_questoes_mat 


#%%
print(dados_enem_mt_questoes_mat[1:2])


#%%

#%%

#%%

#%%













