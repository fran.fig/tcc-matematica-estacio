dados<-read.fwf('https://docs.ufpr.br/~aanjos/TRI/dados/EXAMPL01.DAT',w=c(4,1,rep(1,15)))

dados<-read.fwf('https://docs.ufpr.br/~aanjos/TRI/dados/EXAMPL01.DAT',w=c(4,1,rep(1,15)))[,-2]
head(dados)

gab01<-read.fwf('https://docs.ufpr.br/~aanjos/TRI/dados/EXAMPL01.KEY',w=c(3,2,rep(1,15)))[,-2]
gab01

#install.packages('ltm')
library(ltm)
dados.resp<-mult.choice(dados[,2:16],as.numeric(gab01[,-1]))

#---------------------------------------
saresp<-read.fwf('https://docs.ufpr.br/~aanjos/TRI/dados/manha.dat',widths=c(12,3,rep(1,31)),header=FALSE,skip=1,na.strings=' ')[,-3]
colnames(saresp)<-c('id','turno',paste('i',1:30,sep=""))
head(saresp)
class(saresp)

gabarito<-read.fwf('https://docs.ufpr.br/~aanjos/TRI/dados/manha.dat',widths=c(-16,rep(1,30)),header=F,nr=1,na.string="")
gabarito

require(Deducer)

dados<-recode.variables(saresp,"'A'->1; 'B'->2; 'C'->3; 'D'->4")
names(dados)<-names(saresp) # colocar os nomes do arquivo original
gab2<-recode.variables(gabarito,"'A'->1; 'B'->2; 'C'->3; 'D'->4")

require(ltm)
manha.NA<-mult.choice(dados[,3:32],as.numeric(gab2)) #ltm
head(manha.NA)
manha<-ifelse(is.na(manha.NA)==T,0,manha.NA)

manha.desc<-descript(manha)
names(manha.desc)
manha.desc
plot(manha.desc,ty='b',includeFirstLast=TRUE)
plot(manha.desc,items=c(1:5),ty="b",includeFirstLast=TRUE,pch=c('1','2','3','4','5'))

#install.packages('CTT')
require(CTT)
manha.reliab<-reliability(manha)
manha.reliab$pbis






#---------------------------------------
head(data(LSAT))

descript(LSAT)
fit1<-rasch(LSAT,constraint=cbind(length(LSAT)+1,1))
summary(fit1)

coef(fit1,prob=TRUE, order=TRUE)

fit2<-rasch(LSAT)
summary(fit2)

factor.scores(fit2,met="EAP")
 
factor.scores(fit2, resp.patterns = rbind(c(1,0,1,0,1), c(NA,1,0,NA,1)))

fit3<-ltm(LSAT~z1)
summary(fit3)

manha.tpm<-tpm(manha,control=list(optimizer="nlminb"))

manha.tpm

manha.prof<-factor.scores(manha.tpm)

head(manha.prof$score)


plot(fit2)
plot(fit2,legend=T)
plot(fit2,type='IIC')
plot(fit2,type='IIC',items=0)
plot(factor.scores(fit2,met="EAP"))
