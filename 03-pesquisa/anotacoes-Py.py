
#================================================================================================================================================
elif ANO == 2015:
    mapa_tipos = {
        'NU_INSCRICAO' : 'Int64',
        'NU_ANO' : 'Int16',
        'CO_MUNICIPIO_RESIDENCIA': 'Int32',
        'NO_MUNICIPIO_RESIDENCIA': 'string',
        'CO_UF_RESIDENCIA': 'Int8',
        'SG_UF_RESIDENCIA': 'string',
        'IN_ESTUDA_CLASSE_HOSPITALAR': 'Int8' ,
        'IN_TREINEIRO': 'Int8',
		
        'CO_ESCOLA': 'string',
        'CO_MUNICIPIO_ESC': 'string',
        'NO_MUNICIPIO_ESC': 'string',
        'CO_UF_ESC': 'Int8',
        'SG_UF_ESC':  'string',
        'TP_DEPENDENCIA_ADM_ESC': pd.CategoricalDtype(['1', '2', '3', '4']) ,
        'TP_LOCALIZACAO_ESC': pd.CategoricalDtype(['1', '2']) ,
        'TP_SIT_FUNC_ESC': pd.CategoricalDtype(['1', '2', '3', '4']),

        'NU_IDADE': 'Int8',
        'TP_SEXO': pd.CategoricalDtype(['M','F']),
        'TP_NACIONALIDADE': pd.CategoricalDtype(['0', '1', '2', '3', '4']),
        'CO_MUNICIPIO_NASCIMENTO': 'string',
        'NO_MUNICIPIO_NASCIMENTO': 'string',
        'CO_UF_NASCIMENTO': 'string',
        'SG_UF_NASCIMENTO': 'string',
        'TP_ST_CONCLUSAO': pd.CategoricalDtype(['1', '2', '3', '4']) ,
        'TP_ANO_CONCLUIU': pd.CategoricalDtype(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10']),
        'TP_ESCOLA': pd.CategoricalDtype(['1', '2', '3', '4']),
        'TP_ENSINO': pd.CategoricalDtype(['1', '2', '3']) ,
        'TP_ESTADO_CIVIL': pd.CategoricalDtype(['0', '1', '2', '3']),
        'TP_COR_RACA': pd.CategoricalDtype(['0', '1', '2', '3', '4', '5', '6']),
        'IN_BAIXA_VISAO': 'Int8' ,
		
        'IN_CEGUEIRA': 'Int8' ,
        'IN_SURDEZ': 'Int8' ,
        'IN_DEFICIENCIA_AUDITIVA': 'Int8' ,
        'IN_SURDO_CEGUEIRA': 'Int8' ,
        'IN_DEFICIENCIA_FISICA': 'Int8' ,
        'IN_DEFICIENCIA_MENTAL': 'Int8' ,
        'IN_DEFICIT_ATENCAO': 'Int8' ,
        'IN_DISLEXIA': 'Int8' ,
        'IN_GESTANTE': 'Int8' ,
        'IN_LACTANTE': 'Int8' ,
        'IN_IDOSO': 'Int8' ,
        'IN_DISCALCULIA': 'Int8' ,
        'IN_AUTISMO': 'Int8' ,
        'IN_VISAO_MONOCULAR': 'Int8' ,
        'IN_SABATISTA': 'Int8' ,        #????????????
        'IN_OUTRA_DEF': 'Int8' ,

        'IN_SEM_RECURSO': 'Int8',
        'IN_NOME_SOCIAL': 'Int8',
        'IN_BRAILLE': 'Int8',
        'IN_AMPLIADA_24': 'Int8',
        'IN_AMPLIADA_18': 'Int8',
        'IN_LEDOR': 'Int8',
        'IN_ACESSO': 'Int8',
        'IN_TRANSCRICAO': 'Int8',
        'IN_LIBRAS': 'Int8',
        'IN_LEITURA_LABIAL': 'Int8',
        'IN_MESA_CADEIRA_RODAS': 'Int8',
        'IN_MESA_CADEIRA_SEPARADA': 'Int8',
        'IN_APOIO_PERNA': 'Int8',
        'IN_GUIA_INTERPRETE': 'Int8',
        'IN_MACA': 'Int8',               #????????????
        'IN_COMPUTADOR': 'Int8',
        'IN_CADEIRA_ESPECIAL': 'Int8',
        'IN_CADEIRA_CANHOTO': 'Int8',
        'IN_CADEIRA_ACOLCHOADA': 'Int8',
        'IN_PROVA_DEITADO': 'Int8',
        'IN_MOBILIARIO_OBESO': 'Int8',
        'IN_LAMINA_OVERLAY': 'Int8',
        'IN_PROTETOR_AURICULAR': 'Int8',
        'IN_MEDIDOR_GLICOSE': 'Int8',
        'IN_MAQUINA_BRAILE': 'Int8',
        'IN_SOROBAN': 'Int8',
        'IN_MARCA_PASSO': 'Int8',
        'IN_SONDA': 'Int8',
        'IN_MEDICAMENTOS': 'Int8',
        'IN_SALA_INDIVIDUAL': 'Int8',
        'IN_SALA_ESPECIAL': 'Int8',
        'IN_SALA_ACOMPANHANTE': 'Int8',
        'IN_MOBILIARIO_ESPECIFICO': 'Int8',
        'IN_MATERIAL_ESPECIFICO': 'Int8',

        'IN_CERTIFICADO': 'Int8',                #????????????
        'NO_ENTIDADE_CERTIFICACAO': 'string',    #????????????
        'CO_UF_ENTIDADE_CERTIFICACAO': 'Int8',   #????????????
        'SG_UF_ENTIDADE_CERTIFICACAO': 'string', #????????????
		
        'CO_MUNICIPIO_PROVA': 'Int32',
        'NO_MUNICIPIO_PROVA': 'string',
        'CO_UF_PROVA': 'Int8',
        'SG_UF_PROVA': 'string',
		
        'TP_PRESENCA_CN': pd.CategoricalDtype(['0', '1', '2']),
        'TP_PRESENCA_CH': pd.CategoricalDtype(['0', '1', '2']),
        'TP_PRESENCA_LC': pd.CategoricalDtype(['0', '1', '2']),
        'TP_PRESENCA_MT': pd.CategoricalDtype(['0', '1', '2']), 
        'CO_PROVA_CN': 'Int16',
        'CO_PROVA_CH': 'Int16',
        'CO_PROVA_LC': 'Int16',
        'CO_PROVA_MT': 'Int16',
        'NU_NOTA_CN': 'float64',
        'NU_NOTA_CH': 'float64',
        'NU_NOTA_LC': 'float64',
        'NU_NOTA_MT': 'float64',
        'TX_RESPOSTAS_CN': 'string',
        'TX_RESPOSTAS_CH': 'string',
        'TX_RESPOSTAS_LC': 'string',
        'TX_RESPOSTAS_MT': 'string',
        'TP_LINGUA' : pd.CategoricalDtype(['0', '1']), 
        'TX_GABARITO_CN' : 'string',
        'TX_GABARITO_CH' : 'string',
        'TX_GABARITO_LC' : 'string',
        'TX_GABARITO_MT' : 'string',
		
        'TP_STATUS_REDACAO' : pd.CategoricalDtype(['1', '2', '3', '4', '6', '7', '8', '9', '98']), 
        'NU_NOTA_COMP1': 'float64',
        'NU_NOTA_COMP2': 'float64',
        'NU_NOTA_COMP3': 'float64',
        'NU_NOTA_COMP4': 'float64',
        'NU_NOTA_COMP5': 'float64',
        'NU_NOTA_REDACAO' : 'float64',
		
        'Q001' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']) ,
        'Q002' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']) ,
        'Q003' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F']) ,
        'Q004' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F']) ,
        'Q005' : 'Int8',
        'Q006' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q']) ,
        'Q007' : pd.CategoricalDtype(['A', 'B', 'C', 'D']) ,
        'Q008' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q009' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q010' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q011' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q012' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q013' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q014' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q015' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q016' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q017' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q018' : pd.CategoricalDtype(['A', 'B']) ,
        'Q019' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q020' : pd.CategoricalDtype(['A', 'B']) ,
        'Q021' : pd.CategoricalDtype(['A', 'B']) ,
        'Q022' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q023' : pd.CategoricalDtype(['A', 'B']) ,
        'Q024' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']) ,
        'Q025' : pd.CategoricalDtype(['A', 'B']),
        'Q026' : pd.CategoricalDtype(['A', 'B', 'C']),
        'Q027' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']),
        'Q028' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']),   #????????????
        'Q029' : 'Int8',   #????????????
        'Q030' : 'Int8',   #????????????
        'Q031' : 'Int8',   #????????????
        'Q032' : 'Int8',   #????????????
        'Q033' : 'Int8',   #????????????
        'Q034' : 'Int8',   #????????????
        'Q035' : 'Int8',   #????????????
        'Q036' : 'Int8',   #????????????
        'Q037' : 'Int8',   #????????????
        'Q038' : 'Int8',   #????????????
        'Q039' : 'Int8',   #????????????
        'Q040' : 'Int8',   #????????????
        'Q041' : 'Int8',   #????????????
        'Q042' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']),   #????????????
        'Q043' : pd.CategoricalDtype(['A', 'B', 'C', 'D']),   #????????????
        'Q044' : pd.CategoricalDtype(['A', 'B', 'C']),   #????????????
        'Q045' : pd.CategoricalDtype(['A', 'B', 'C', 'D']),   #????????????
        'Q046' : pd.CategoricalDtype(['A', 'B', 'C', 'D']),   #????????????
        'Q047' : pd.CategoricalDtype(['A', 'B', 'C', 'D', 'E']),   #????????????
        'Q048' : pd.CategoricalDtype(['A', 'B', 'C', 'D']),   #????????????
        'Q049' : pd.CategoricalDtype(['A', 'B', 'C']),   #????????????
        'Q050' : pd.CategoricalDtype(['A', 'B', 'C', 'D']),   #????????????
    } 