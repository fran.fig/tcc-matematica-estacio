# Prof. Luis Anunciação (PUC-Rio / ANOVA)
# Material complementar (anovabr.com)
# Use à vonatde

# baseline
# ter os arquivos no R!

#passo 1: avaliar a dimensionalidade do instrumento
library(psych)
scree(tri)
fa.parallel(tri)

#x <- fa(tri, nfactors = 1, rotate = "varimax", cor = "poly")
#x
fa.plot(x, cut = 0.9)

#passo 2: criar/rodar os modelos 
library(mirt)
mirtCluster()

mod1 <- mirt(tri, 1, itemtype = 'graded')
mod2 <- mirt(tri, 1, itemtype = 'gpcm') #modelo não fechou!
mod3 <- mirt(tri, 1, itemtype = 'rsm')

#fit do modelo
M2(mod1)
M2(mod2)
M2(mod3)

#comparar modelos
anova(mod1, mod2)
anova(mod2, mod3)

# Analisar os itens
coef(mod1, simplify = TRUE)
summary(mod1, suppress = .31)
itemfit(mod1) # H0 = item é bom! Aqui, o ideal é que p > 0.05!

#plotar
plot(mod1) #valor total esperado
plot(mod1, type = 'SE') # erro
plot(mod1, type = 'info') #informação do teste
plot(mod1, type = 'trace') #todos os itens Modelos unidimensionais!

itemplot(mod1, 1) # curva de cada item dado as categorias
itemplot(mod1, 8) #item 8
itemplot(mod1, 1, type = 'score') # score de cada item
itemplot(mod1, 8, type = 'infotrace', shiny = TRUE) # score de cada item

# Veja mais material de Psicometria e Estatística em anovabr.com 